// Copyright (C) 2012 - 2013 by Pedro Mendes, Virginia Tech Intellectual
// Properties, Inc., University of Heidelberg, and The University
// of Manchester.
// All rights reserved.

#ifndef COPASI_VERSION
#define COPASI_VERSION

#define COPASI_VERSION_MAJOR 4
#define COPASI_VERSION_MINOR 22
#define COPASI_VERSION_BUILD 170
#define COPASI_VERSION_MODIFIED false
#define COPASI_VERSION_COMPATIBILITY {170}
#define COPASI_VERSION_COMMENT "stable"
#define COPASI_VERSION_CREATOR "copasi.org"

#endif // COPASI_VERSION
