// Copyright (C) 2017 by Pedro Mendes, Virginia Tech Intellectual
// Properties, Inc., University of Heidelberg, and University of
// of Connecticut School of Medicine.
// All rights reserved.

// Copyright (C) 2010 - 2016 by Pedro Mendes, Virginia Tech Intellectual
// Properties, Inc., University of Heidelberg, and The University
// of Manchester.
// All rights reserved.

// Copyright (C) 2008 - 2009 by Pedro Mendes, Virginia Tech Intellectual
// Properties, Inc., EML Research, gGmbH, University of Heidelberg,
// and The University of Manchester.
// All rights reserved.

// Copyright (C) 2004 - 2007 by Pedro Mendes, Virginia Tech Intellectual
// Properties, Inc. and EML Research, gGmbH.
// All rights reserved.

/*!
    \file CAnnotatedMatrix.h
    \brief Header file of class CArrayAnnotation
 */

#ifndef CANNOTATEDMATRIX_H
#define CANNOTATEDMATRIX_H

#include "report/CCopasiContainer.h"
#include "report/CCopasiObjectName.h"

#include "report/CCopasiArray.h"
#include <string>

#include "copasi.h"

#include "utilities/CCopasiVector.h"

/**
 * This class contains the annotations to a n-dimensional array. Annotations can be provided
 * for the array as such, for each of the dimensions, and for each of the indices (rows,
 * columns, ...)
 */
class CArrayAnnotation: public CCopasiContainer
{
public:
  typedef C_FLOAT64 data_type;
  typedef CCopasiArray::index_type index_type;
  typedef std::vector< CRegisteredObjectName > name_index_type;

  /**
   * The annotation to an array can work in different modes. The mode
   * for each dimension can be changed independently.
   *
   * OBJECTS: The CNs for the rows, cols, ... of the array are given explicitly
   *
   * VECTOR: A CCopasiVector is provided from which the CNs are generated at
   * the time the vector is set
   *
   * VECTOR_ON_THE_FLY: A CCopasiVector is provided, but the CNs are generated
   * every time the annotations are retrieved (so that they are always up to date).
   *
   * STRINGS: The annotations for the rows, cols, ... are given explicitly
   * as strings.
   *
   * NUMBERS: The rows, cols, ... of the array are only annotation with
   * running numbers (starting with 0)-
   */
  enum Mode
  {
    OBJECTS,
    VECTOR,
    VECTOR_ON_THE_FLY,
    STRINGS,
    NUMBERS
  };

private:
  CArrayAnnotation();
  CArrayAnnotation(const CArrayAnnotation &);
  CArrayAnnotation & operator=(const CArrayAnnotation &);

public:
  CArrayAnnotation(const std::string & name,
                   const CCopasiContainer * pParent,
                   CCopasiAbstractArray * array,
                   const bool & adopt);

  virtual ~CArrayAnnotation();

  /**
   *  let the ArrayAnnotation point to a different array.
   *  If you use this method without updating the annotations afterwards
   *  it is your responsibility to make sure the new array fits the
   *  existing annotation (in dimensionality and, if not in VECTOR_ON_THE_FLY mode,
   *  in size).
   */
  void setArray(CCopasiAbstractArray * a);

  CCopasiAbstractArray * array()
  {return mpArray;}

  const CCopasiAbstractArray * array() const
  {return mpArray;}

  /**
   * set the mode for the dimension d
   */
  void setMode(size_t d, Mode m);

  /**
   * set the mode for all dimensions, this also sets the
   * default mode that is used when resizing the ArrayAnnotion
   * to a larger dimensionality
   */
  void setMode(Mode m);

  Mode getMode(size_t d) const
  {return mModes[d];};

  Mode getDefaultMode() const
  {return mDefaultMode;};

  size_t dimensionality() const;

  CCopasiAbstractArray::index_type size() const
  {return mpArray->size();}

  /**
   * Associates a dimension d of the array with a CCopasiVector of
   * CCopasiObjects. If the mode of the dimension d is VECTOR than
   * the CNs of the objects in the vector are generated and stored immediately.
   * If the mode is VECTOR_ON_THE_FLY the CNs are generated when getAnnotationsCN()
   * or getAnnotationsString() is called.
   */
  template < class CType > void setCopasiVector(size_t d, const CCopasiVector< CType > & v)
  {
    assert(d < dimensionality());
    assert((mModes[d] == VECTOR) || (mModes[d] == VECTOR_ON_THE_FLY));

    size_t i;

    for (i = 0; i < mAnnotationsCN[d].size() && i < v.size(); ++i)
      {
        mAnnotationsCN[d][i] = v[i].getCN();

        // The annotation string is always updated when retrieved since objects may be renamed
        // Thus we do not update them here
      }

    return;
  }

  void setAnnotation(size_t d, size_t i, const CCopasiObject * pObject);
  void setAnnotationString(size_t d, size_t i, const std::string s);

  /**
   * returns the vector of CNs that correspond to the rows, columns, ... of the array.
   * This method must not be called if the mode for the dimension d is STRINGS or NUMBERS
   */
  const std::vector<CRegisteredObjectName> & getAnnotationsCN(size_t d) const;

  /**
   * This returns strings that annotate the rows, columns, ... of the array.
   * If the mode is OBJECTS, VECTOR, or VECTOR_ON_THE_FLY the display argument determines
   * if the object name or the object display name is used.
   * Note that this method returns a reference. The content that the reference points to
   * may be changes by later calls to the getAnnotationsCN() method with the same value for d.
   * Specifically if you use this method to obtain a reference to the list of display names
   * and then call the method again to get the plain object names, the first reference will
   * after that also point to the plain object names.
   */
  const std::vector<std::string> & getAnnotationsString(size_t d, bool display = true) const;

  const std::string & getDimensionDescription(size_t d) const;
  void setDimensionDescription(size_t d, const std::string & s);

  const std::string & getDescription() const;
  void setDescription(const std::string & s);

  /**
   * adjust the dimensionality and size to that of the array
   */
  void resize();

  /**
   * an object that will act as a reference to one element of an array
   * will be created and inserted as a child of the array annotation. Basically
   * most of the work will be done by getObject(). If the element already
   * exists, the existing element will be returned.
   */
  const CCopasiObject * addElementReference(const index_type & index) const;
  const CCopasiObject * addElementReference(const name_index_type & nameIndex) const;

  /**
   * a convenience function for 2-dimensional arrays.
   */
  const CCopasiObject * addElementReference(C_INT32 u, C_INT32 v) const;

  /**
   * a convenience function for 1-dimensional arrays.
   */
  const CCopasiObject * addElementReference(C_INT32 u) const;

  /**
   * Resolve a cn. Since this is an array, the CN can start with an index like "[2][3]".
   * Since this is also a container, this is not necessarily the case.
   */
  virtual const CObjectInterface * getObject(const CCopasiObjectName & cn) const;

  /**
   * Check whether the size of array is greater than 0 for each dimension.
   * Return true, if so. Otherwise, false.
   */
  bool isEmpty();

//private:
  /**
   *  resize the internal vectors according to the dimensionality of the array
   */
  void reDimensionalize(size_t d);

  void resizeOneDimension(size_t d);

  //void printDebugLoop(std::ostream & out, CCopasiAbstractArray::index_type & index, size_t level) const;

  //void printDebug(std::ostream & out) const;

  void printRecursively(std::ostream & ostream, size_t level,
                        CCopasiAbstractArray::index_type & index,
                        const std::vector<std::vector<std::string> > & display) const;

  /**
   * generate a display name for the array annotation.
   */
  virtual std::string getObjectDisplayName() const;

  virtual void print(std::ostream * ostream) const;

  friend std::ostream &operator<<(std::ostream &os, const CArrayAnnotation & o);

  data_type & operator[](const name_index_type & nameIndex);
  const data_type & operator[](const name_index_type & nameIndex) const;

  CCopasiAbstractArray* getArray() const;

  name_index_type displayNamesToCN(const std::vector< std::string > & DisplayNames) const;
  index_type cnToIndex(const name_index_type & cnIndex) const;
private:
  std::string createDisplayName(const std::string & cn) const;
  void updateDisplayNames() const;

  CCopasiAbstractArray * mpArray;
  bool mDestructArray;

  std::vector< std::vector<CRegisteredObjectName> > mAnnotationsCN;
  mutable std::vector< std::vector<std::string> > mAnnotationsString;

  std::vector< std::string > mDimensionDescriptions;

  /**
   * This contains the mode for the different dimensions
   */
  std::vector<Mode> mModes;

  /**
   * This contains the default mode that is used if during a resize()
   * the dimensionality is increased.
   */
  Mode mDefaultMode;

  std::string mDescription;
};

#endif
